#!/usr/bin/env python

import RPi.GPIO as GPIO, time

GPIO.setmode(GPIO.BCM)
LED = 18
Button = 23
GPIO.setup(LED, GPIO.OUT)
GPIO.setup(Button, GPIO.IN)
light = True

while True:
	input = GPIO.input(Button)
	if light == True:
		GPIO.output(LED, True)
	else:
		GPIO.output(LED, False)
	if input == True and light == False:
		light == True
		print 'True'
	if input == True and light == True:
		light == False
		print 'False'